#!/bin/sh

## Clear out the funky old paths
PATH=$(echo $PATH | sed -e 's/\/opt\/mathematica.*://')

cd @@INSTDIR@@/Executables
./mathematica $*
