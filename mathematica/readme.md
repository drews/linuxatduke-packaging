This is the Linux @ Duke packaging for the commercial Mathematica application.

# Building
One must first obtain the Mathematica download and then run their installer,
`Mathematica_10.2.0_LINUX.sh` , but do 
not install anything. Upon running the installer, it will extract itself into a dot directory 
which one tars up to create `mathematica10-10.2.0.tar.gz` which is a source for the rpmbuild 
process.

This is a change
